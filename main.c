#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#define ITER 1000000000

double distdaorigem(double x, double y);

int main() {
	/* Ao = pi*r² */
	/* Para facilitar os cálculos, usaremos um quarto do círculo, com
	 * centro em (0,0) e utilizando apenas o primeiro quadrante. */
	int circ = 0;
	double pi = 0;

	srand(time(NULL));

	for(int j = 0; j < 100; j++) {
		printf("\rProcessando... (%d%%)", j);
		fflush(stdout); /* Se não usar fflush, apenas *
						 * aparece o último printf(). */
		for(int i = 0; i < (ITER/100); i++) {
			double px, py;
			px = (float) rand() /(float) RAND_MAX;
			py = (float) rand() /(float) RAND_MAX;

			if(distdaorigem(px, py) <= 1) circ++;
		}
	}

	/* Se Ao = pi*r², e r = 1, Ao = pi. Como só estamos considerando o primeiro
	 * quadrante, apenas um quarto da área é calculada. Por isso, multiplicamos
	 * o valor por 4. */

	printf("\rResultado: Pi = %f\n",
			((float) 4 * (float) circ / (float) ITER)
		  );
}

double distdaorigem(double x, double y) {
	return sqrt(x*x + y*y);
}
